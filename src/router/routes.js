const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: "movie-list",
        component: () => import("pages/movie/List.vue"),
      },
      {
        path: "create",
        name: "movie-create",
        component: () => import("pages/movie/Form.vue"),
      },
      {
        path: "update/:id",
        name: "movie-update",
        component: () => import("pages/movie/Form.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
