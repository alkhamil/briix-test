export function createMovieList(state, data) {
  data.id = state.movieList.length + 1;
  state.movieList = [...state.movieList, data];
}

export function updateMovieList(state, data) {
  const { item, movieId } = data;
  const itemToUpdate = state.movieList.find(
    (item) => item.id.toString() === movieId.toString()
  );

  Object.assign(itemToUpdate, item);
}

export function deleteMovie(state, movieId) {
  let index = state.movieList.findIndex(
    (item) => item.id.toString() === movieId.toString()
  );
  state.movieList.splice(index, 1);
}
