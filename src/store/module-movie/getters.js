export function movieForm(state) {
  return state.movieForm;
}

export function movieList(state) {
  return state.movieList;
}
